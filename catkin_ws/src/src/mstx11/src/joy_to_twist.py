#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

    # Author: Andrew Dai
    # This ROS Node converts Joystick inputs from the joy node
    # into commands for turtlesim

    # Receives joystick messages (subscribed to Joy topic)
    # then converts the joysick inputs into Twist commands
    # Edited

def callback(data):
	twist = Twist()
# Map this stuff yourself, because you lost the card which had everything on it
# And the joystick is in the boat so I can't do it for you
        twist.linear.x = data.axes[1]
        twist.lineair.y = data.axes[0]
	twist.lineair.z = ddata.axes[2]
        pub.publish(twist)

    # Intializes everything
def start():
        global pub
        pub = rospy.Publisher('/msxt11/cmd_vel', Twist)
        # subscribed to joystick inputs on topic "joy"
        rospy.Subscriber("joy", Joy, callback)
        # starts the node
        rospy.init_node('JoyToTwist')
        rospy.spin()

if __name__ == '__main__':
        start()

