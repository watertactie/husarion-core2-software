//SOFTWARE//
Software for the husarion core2 board and for the connected Raspberry Pi
Raspberry Pi login: "husarion", password: "husarion"
Local SSH ip now:172.31.81.104 (This is on Innovation Dock WiFI, no guarantee it's still the same ip addres, but cloud.husarion.com always works)
Gerard Simmons has the husarion account that's linked to the core 2, but this can easily be changed.


If the micro SD card isn't found:
Use the hConfig app on your phone to connect to the husarion wifi hotsput and follow the onscreen instructions. (if not using Innovatio Dock WiFi)
WiFi on boat is "NSA NETWORK" with "waves&sun" as password
Follow http://wiki.ros.org/joy/Tutorials/ConfiguringALinuxJoystick to configure the joystick for this pi
Change the converter file from joystick messages to twist message, so it suits the joystick. (~/catkin_ws/src/mstx11/src/joy_to_twist.py)
I put the gas value in twist.x, steering value in twist.y and the twist.z, look in the husarion core2 source code for more information.

Goto if the micro SD card is found!, after completing the above


If the micro SD card is found!
Goto cloud.husarion.com and login.
From here got to the ssh tab of the Pi.
To start communication between the Raspberry Pi and the Core 2 board, got to SSH and type the following:
"roslaunch mstx11 mstx11.launch"
and on a new line "/opt/husarion/tools/rpi-linux/ros-core2-client /dev/serial0"  (not sure if this was the correct one and can test it because the computer already has been butchered)


//HARDWARE//
The hardware connections are labeled. The ground wires that are taped away can be cut off because the fulfil no purpose.
Quite sure the DAC is dead and needs to be replaced (the red Sparkfun board)
If you get a hard beep from the engine control module(ECM) on the boat, that means that there is no correct voltage applied to the ECM.
Turn of the motor and turn it back on when your confident that you have corrected the mistake.