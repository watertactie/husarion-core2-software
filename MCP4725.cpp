#include "MCP4725.h"


MCP4725::MCP4725() {
}

void MCP4725::begin(uint8_t addr) {
	_i2caddr = addr;
	hExt.i2c.selectI2C();
	hExt.i2c.setDataRate(400000);
}


void MCP4725::setVoltage(uint16_t output)
{
	/*if (output <= 400)
	{
	output = 400;
	}*/
	output = 500;
	uint8_t data[] = { 0x40, output / 16,(output % 16) << 4 };
	hExt.i2c.write(_i2caddr, data, 3);

}