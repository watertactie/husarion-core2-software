#include "hFramework.h"
//#include "hI2C.h"


class MCP4725 {
public:
	MCP4725();
	void begin(uint8_t a);
	void setVoltage(uint16_t output);

private:
	uint8_t _i2caddr;
};