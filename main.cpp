#include "cstddef"
#include "cstdint"
#include "hFramework.h"
#include "hCloudClient.h"
#include "ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include "MCP4725.h"


using namespace hFramework;
ros::NodeHandle nh; // Required for ROS communication
MCP4725 dac; // Required for DAC library
float gas, nozzle, bucket; // Declare global

void twistCallback(const geometry_msgs::Twist &twist)
{
	LED3.toggle();
	gas = twist.linear.x;
	nozzle = twist.linear.y;
	bucket = twist.linear.z;
	//platform.ui.label("lb_gas").setText("%f [V]", gas);
	//platform.ui.label("lb_nozzle").setText("%f [V]", nozzle);
	//platform.ui.label("lb_bucket").setText("%f [V]", bucket);
}
///////////////////////////////////////////////////////////////////////////////////////////////
void cfgHandler()
{
	//uncomment if you want to stream video from your project
	//platform.ui.video.enableTransmission();
	platform.ui.loadHtml({ Resource::WEBIDE, "/ui.html" });
}
///////////////////////////////////////////////////////////////////////////////////////////////
void checkADC()
{
	hExt.pin3.enableADC(); // enable ADC on pin1 on hExt port
	hExt.pin4.enableADC(); // enable ADC on pin1 on hExt port
	hExt.pin5.enableADC(); // enable ADC on pin1 on hExt port
	float val1 = hExt.pin3.analogReadVoltage(); // read analog value (voltage in [V])
	float val2 = hExt.pin4.analogReadVoltage(); // read analog value (voltage in [V])
	float val3 = hExt.pin5.analogReadVoltage(); // read analog value (voltage in [V])
	// WEBSITE
	//platform.ui.label("lb_adc0").setText("%f [V]", val1);
	//platform.ui.label("lb_adc1").setText("%f [V]", val2);
	//platform.ui.label("lb_adc2").setText("%f [V]", val3);
}
///////////////////////////////////////////////////////////////////////////////////////////////
void setDAC(int value)
{
	dac.setVoltage(value);
}
///////////////////////////////////////////////////////////////////////////////////////////////
void setHBridge(int value, bool direction)
{

	hServoModule.enablePower();
	hServoModule.setVoltage5V();
	hServoModule.servo2.setPeriod(20000);
	hServoModule.servo3.setPeriod(20000);
	if (value != 0) {
		if (direction == true)
		{
			hServoModule.servo2.setWidth(value * 20000); //set pulse width: 1000us  
			hServoModule.servo3.setWidth(0);
			// Steer to the left    
		}
		if (direction == false)
		{
			hServoModule.servo2.setWidth(0); //set pulse width: 1000us 
			hServoModule.servo3.setWidth(value * 20000);
			// Steer to the right
		}
	}
	else {
		hServoModule.servo2.setWidth(10);
		hServoModule.servo3.setWidth(10);
	}

}
//////NOT USED ///////////////////////////////////////////////////////////////////////////////////
void controlNozzle(float nozzle)
{
	hExt.pin4.enableADC(); // enable ADC on pin1 on hExt port
	float nozzlePos = hExt.pin4.analogReadVoltage(); // read analog value (voltage in [V])
													 // Temp
	if (nozzlePos < 300) {
		nozzlePos = 300;
	}
	else if (nozzlePos > 3795) {
		nozzlePos = 3795;
	}
	float modNozzlePos = (nozzlePos - 300) / 3495; // Map valueas from 300 <-> 3795 to 0 <-> 1
												   // End Temp
	if (modNozzlePos == nozzle) {
		setHBridge(0, true); // max 50% speed
	}
	else if (modNozzlePos < nozzle) {
		setHBridge(20000, true); // 25% speed
	}

	else if (modNozzlePos > nozzle) {
		setHBridge(20000, false); // 25% speed
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////
ros::Subscriber<geometry_msgs::Twist> sub("/mstx11/cmd_vel", &twistCallback);


void hMain()
{
	dac.begin(0x60);
	platform.begin(&RPi);
	nh.getHardware()->initWithDevice(&platform.LocalSerial);
	nh.initNode();
	nh.subscribe(sub);
	platform.ui.configHandler = cfgHandler;
	platform.ui.setProjectId("@@@PROJECT_ID@@@");
	sys.setSysLogDev(&devNull); //turn off sys logs
	for (;;) {
		//platform.ui.label("lb_bat").setText("%f [V]", sys.getSupplyVoltage());
		if (nozzle > 0)
		{
		setHBridge(nozzle*20000, true);
		}
		else if (nozzle < 0)
		{
		//gas = abs(gas);
		setHBridge(nozzle*20000, false);
		}
		else
		{
		setHBridge(0, false);
		}

		//controlNozzle(gas);
		checkADC();
		setDAC(gas * 3500);
		nh.spinOnce();
		sys.delay(10);
	}
}
